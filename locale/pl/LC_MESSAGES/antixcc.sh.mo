��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            2  3     f     x     �     �     �     �     �     �     �          &     9     Y     j     �     �  (   �  #   �     �       /   5     e     }      �     �  
   �     �  !   �     �          ,     F     ^     t     �     �     �     �     �     �     �     
  &        F     R     i  !   u     �     �     �  +   �     �     
     "     7     M     f     |     �     �     �     �     �     �     
       2   4     g  %     "   �     �     �     �               #     :     I     \     x      �  #   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:26+0300
Last-Translator: Filip Bog <mxlinuxpl@gmail.com>
Language-Team: Polish (http://www.transifex.com/anticapitalista/antix-development/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 2.3
  DisconnectShares 1-to-1 Assistance 1-to-1 Voice Konfiguracja ADSL/PPPOE Adblock Dostosuj mikser dźwięku Korektor Alsamixer Konfigurator alternatyw Jasność podświetlenia Naprawa rozruchu Menedżer Broadcom Zmień mapę klawiszy dla sesji Zmień tło Slim Wybór usług startowych Wybierz tapetę pulpitu Instalator kodeków Konfigurowanie automatycznego montowania Konfiguracja połączenia GPRS/UMTS Konfiguracja Live Persistence Konfiguracja Connectshares Utwórz Live-USB Retain Partitions (UNetbootin) Dostosuj wygląd i styl Pulpit Konfiguracja Dial-Up (GNOME PPP) Dyski Sterowniki Droopy (Udostępnianie plików) Edytuj menu programu rozruchowego Edytuj pliki konfiguracyjne Edytuj pliki wykluczeń Edytuj ustawienia Fluxbox Edytuj ustawienia IceWM Edytuj ustawienia JWM Edytuj monitor systemu (Conky) Konfiguracja zapory Sprzęt Zrzut dysku Utwórz obraz partycji Zainstaluj system antiX Linux Live Live USB Maker (CLI) Live USB Maker (GUI) Aktualizator jądra (kernela) Live-USB Konserwacja Zarządzanie pakietami Edytor menu Zamontuj podłączone urządzenia Konfiguracja myszy Sieć Asystent sieci Konfiguracja interfejsów sieciowych (ceni) Instalator sterowników Nvidia Informacje o komputerze Instalator pakietów Partycjonowanie dysku Monit o hasło (su/sudo) Preferowane aplikacje Ustawienia drukowania Remasteruj-Dostosuj Live Menedżer repozytoriów SSH Conduit Zapisz Root Persistence Sesja Ustaw automatyczne logowanie Ustaw datę i czas Ustaw rozmiar czcionki Ustaw obrazek startowy dla GRUB (tylko format png) Ustaw wygaszanie ekranu Ustaw rozdzielczość ekranu (ARandR) Ustaw układ klawiatury systemowej Ustaw Live Persistence Udziały Wybór karty dźwiękowej Synchronizacja katalogów System Kopia zapasowa systemu Test dźwięku Sesja użytkownika Zarządzanie użytkownikiem Konfiguracja WPA Supplicant Połącz bezprzewodowo (Connman) Sterowniki bezprzewodowe MS Windows 