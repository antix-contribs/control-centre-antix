��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �     �          )     7     J  "   c     �     �     �  '   �  !   �  #        3     F  '   `     �  +   �     �  *   �          *  #   6     Z     `     g  $   ~     �     �     �     �       (   .     W  
   l     w     �     �     �  (   �  5   �       	   0     :     M     ^     w     �     �      �     �     �     �     �          &     ;  *   D     o     �  3   �     �     �     �          %     C  "   \  -     $   �  	   �     �     �               !     /     M     b      p     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:25+0300
Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 DisconnectShares Etäkäyttö 1-to-1-yksiyhteen ääni ADSL/PPPOE-asetukset Mainostenesto Säädä mikseriä Alsamixer taajuuskorjain Vaihtoehtoisuuksien asetustyökalu Taustavalojen kirkkaus Käynnistyksen korjaus Broadcom Manager Muuta istunnon näppäimistöasettelua  Vaihda hoikkatyyppinen taustakuva Valitse käynnistettävät palvelut Valitse taustakuva Koodekkien asennusohjelma Määrittele automaattinen liittäminen Konfiguroi GPRS/UMTS Määrittele Live:n persistenssi-tilavaraus ConnectShares-asetukset Luo Live-USB:n säilytysosiot (UNetbootin) Mukauta ulkoasua ja fiilistä Työpöytä Puhelinmodeemiasetukset (GNOME PPP) Levyt Ajurit Droopy (tiedostonjako) Muokkaa käynnistyslataimen valikkoa Muokkaa konfiguraatiotiedostoja Muokkaa ulosrajaustiedostoja Muokkaa Fluxboxin asetuksia Muokkaa IceWM:n asetuksia Muokkaa JWM:n asetuksia Muokkaa järjestelmän valvontaa (Conky) Palomuurin asetukset Laitteisto ISO Tilannevedos Tee osiosta levykuva Asenna antiX Linux Live Live USB:n tekijä (cli, komentokehote)  Live USB:n tekijä (GUI, graafinen käyttöliittymä) Live-USB kernelin päivitys Ylläpito Hallitse paketteja Valikkomuokkaaja Liitä kytketyt laitteet Konfiguroi hiiri Verkko Verkkoavustin Verkon käyttöliittymät (Ceni) Nvidia-ajurien asennus Tietokoneen tiedot Pakettien asennus Osioi kiintolevy Salasanan kysely (su/sudo) Suosimat sovellukset Tulostus Live:n uudelleenisännöinti-mukauttaminen Pakettivarastojen hallinta SSH-välitys Tallenna root-juurikäytön persistenssi-tilavaraus Istunto Aseta automaattikirjautuminen Aseta päiväys ja kellonaika Aseta kirjasimen koko Grub:in taustakuva (vain png) Aseta näytön tyhjennys Aseta näytön resoluutio (ARandR) Aseta järjestelmän näppäimistösijoittelu Aseta Live:n persistenssi-tilavaraus Jakaminen Äänikortin valinta Synkronoi hakemistot Järjestelmä Varmuuskopio Testaa ääni Käyttäjätyöpöytäistunto Käyttäjänhallinta WPA-asetukset Yhdistä langattomasti (Connman) MS Windows langattomat ajurit 