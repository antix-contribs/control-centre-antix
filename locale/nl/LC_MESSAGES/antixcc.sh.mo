��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �               *     2     P     d     ~     �     �  '   �     �     �            #   .      R     s     �  .   �     �     �     �          $     ,     D     [     s     �     �     �     �     �               *     =     T     Y     n     �  	   �     �     �     �     �     �     �     	     $     @     N     c     w     �     �     �     �     �     �     	          $     2  (   G     p      �     �     �  	   �     �                    .     :     S     d     �     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:26+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 SharesLoskoppelen 1-op-1 Assistentie 1-op-1 Voice ADSL/PPPOE Configuratie Adblock Instelling Wijzigen van Mixer Alsamixer Equalizer Alternatives Configurator Backlight Helderheid Bootreparatie Broadcom Beheer Verander Toetsenbord Layout voor Sessie Verander Slim-Achtergrond Kies Opstartdiensten Kies Achtergrond Codecs Installeerder Configureer Automatisch Aankoppelen Configureer GPRS/UMTS-Verbinding Configureer Live Persistence ConnectShares Configuratie Creëer Live-USB Behoud Partities (UNetbootin) Look and Feel Aanpassen Desktop Inbelconfiguratie (GNOME PPP) Opslagmedia Drivers Droopy (Bestandsdeling) Bewerk Bootloader Menu Bewerk Config-Bestanden Bewerk Exclude Bestanden Bewerk Fluxbox-Instellingen IceWM instellingen bewerken Jwm Instellingen Bewerken Bewerken System Monitor (Conky) Firewall Configuratie Hardware ISO Snapshot Partitie Weergeven Installeer antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Onderhoud Pakketbeheer Menu Editor Aangesloten Apparaten Opstarten Muisconfiguratie Netwerk Netwerk Assistent Netwerkverbindingen (Ceni) Nvidia Driver Installeerder PC informatie Pakket Installeerder Partitioneer Schijf Password Prompt (su/sudo) voorkeurstoepassingen Afdrukinstellingen Remaster-Customize Live Pakketbronbeheer SSH Conduit Root Persistentie Opslaan Session Stel Auto-Login In Datum en Tijd Stel Font Grootte In Stel Grub Boot Afbeelding In (enkel png) Stel Screen blanking in Stel Schermresolutie In (ARandR) Stel Systeem Keyboard Layout In Live Persistentie Instellen Gedeelden Geluidskaart Kiezer Bestanden Afstemmen Systeem Systeem Backup Test geluid Gebruiker Desktop-sessie Gebruikersbeheer WPA Supplicant Configuratie WiFi Verbinden (Connman) Windows Draadloos Drivers 