��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �     �                    ,     ?     O     b     o          �     �     �     �     �     �     	     %  &   9     `     p     w     �     �     �     �     �     �     �                /     P     `  
   g     r     �     �     �     �     �     �     �               4     A     H     U     h     �     �     �     �     �     �     �       
        #     <     C     V     l  #        �     �     �     �                    %     2     ?     L     _     o     �     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:20+0300
Last-Translator: Ivan <personal@live.hk>
Language-Team: Chinese (Taiwan) (http://www.transifex.com/anticapitalista/antix-development/language/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.3
 斷開共享 一對一協助 一對一語音 ADSL/PPPOE 組態 Adblock 調整混音器 Alsamixer均衡器 替代配置器 背光亮度控制 開機修復 Broadcom 管理 更改會話的鍵映射 更改苗條背景 選擇啟動服務 選擇牆紙 影音編碼安裝工具 配置自動掛載 配置GPRS / UMTS 設定現場版的持續性 ConnectShares配置 創建Live-USB保留分區(UNetbootin) 自定義外觀 桌面 撥號配置 (GNOME PPP) 磁碟 驅動程式 Droopy (文件共享) 編輯引導程序菜單 編輯配置文件 編輯排除文件 編輯Fluxbox設置 編輯IceWM設置 編輯JWM設置 編輯系統監視器（Conky） 防火牆配置 硬體 ISO 快照 映像一個分區 安裝antiX Linux 生活 Live USB 製作 (指令介面) Live USB 製作 (圖形介面) Live-USB內核更新器 保養 管理軟體包 選單編輯工具 掛載連接的設備 鼠標配置 網路 網路助理 網絡介面(Ceni) Nvidia驅動程序安裝程序 電腦資訊 軟體安裝工具 硬盤分區 密碼提示 (su/sudo) 首選應用 列印設定 重新製作-自定義現場 軟體倉庫管理員 SSH 驅動 儲存 root 的持續性 會話 設置自動登錄 設定日期和時間 設定文字號數 設置Grub啟動圖片 (只可 png) 設置屏幕消隱 設定螢幕解析度 (ARandR) 設置系統鍵盤佈局 配置恆常操作 分享 聲卡選擇器 同步目錄 系統工具 系統備份 測試聲音 用戶桌面會話 使用者管理 WPA Supplicant 配置 WiFi連接(Connman) Windows無線驅動程序 