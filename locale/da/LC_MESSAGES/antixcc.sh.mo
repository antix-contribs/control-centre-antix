��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �               /     @     M     a     |     �     �      �     �     �               9     Y     n     �  -   �     �  
   �  )   �     '     -     5     H     g     �     �     �     �  "   �          )     2     G     d     z       %   �     �     �     �     �          (     :     C     V  &   t     �     �     �     �     �     
     $     9     R     ^     q     y     �     �  &   �     �  !         (     I     a     j     y     �     �     �     �     �     �                   *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:24+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  DisconnectShares 1-til-1 assistance 1-til-1 stemme ADSL-/PPPOE-konfiguration Reklameblokering Juster mixer Alsamixer-equalizer Alternativer-konfiguration Baggrundsbelysningens lysstyrke Opstartsreparation Broadcom-håndtering Skift tastaturlayout for session Skift Slim-baggrund Vælg opstartstjenester Vælg tapet Codecs-installationsprogram Konfigurer automatisk montering Konfigurer GPRS/UMTS Konfigurer live-vedvarenhed Connectshares-konfiguration Opret Live-USB bevar-partitioner (UNetbootin) Tilpas udseende og fremtoning Skrivebord Opkaldsnetværk-konfiguration (GNOME PPP) Diske Drivere Droopy (fildeling) Rediger opstartsindlæser-menu Rediger konfigurationsfiler Rediger ekskluder-filer Rediger Fluxbox-indstillinger Rediger IceWM-indstillinger Rediger JWM-indstillinger Rediger systemovervågning (Conky) Firewall-konfiguration Hardware ISO-øjebliksbillede Opret aftryk af en partition Installer antiX Linux Live Live USB-skaber (kommandolinje) Live USB-skaber (grafisk brugerflade) Live-USB kerneopdatering Vedligeholdelse Håndter pakker Menuredigering Monter tilsluttede enheder Musekonfiguration Netværk Netværksassistent Netværksgrænseflader (Ceni) Installationsprogram til Nvidia-driver PC-information Pakkeinstallationsprogram Partitioner et drev Adgangskodeprompt (su/sudo) Foretrukne programmer Udskrivningsindstillinger Remaster-tilpas live Softwarekildehåndtering SSH conduit Gem rodvedvarenhed Session Indstil automatisk login Indstil dato og klokkeslæt Indstil skriftstørrelse Indstil Grub-opstartsbillede (kun png) Indstil skærmblankning Indstil skærmopløsning (ARandR) Indstil systemets tastaturlayout Opsæt live-vedvarenhed Delinger Lydkortvælger Synkroniser mapper System Systemsikkerhedskopiering Test lyd Brugerens skrivebordssession Brugerhåndtering WPA supplikant-konfiguration WiFi-forbindelse (Connman) Windows trådløse drivere 