��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3  %   �       	             :     B     ]     q     �     �     �  (   �     �          8     L  !   d     �  "   �  ,   �  6   �     $  
   >  -   I     w          �      �     �  .   �          3     O  #   f  #   �     �     �     �     �     �     �  "     &   :     a     h     y     �     �     �     �     �      �               &     E     c     }  $   �     �  
   �     �     �     �     	       %   8     ^  +   u     �     �     �     �     �     	                ,     K      c     �     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 13:39+0300
Last-Translator: Toprak Bülbül <toprakbulbulv5@gmail.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Paylaşımların bağlantısını kes 1-e-1_yardım 1-e-1_ses ADSL/PPPOE yapılandırması Adblock Karıştırıcıyı Ayarla Alsamixer Ekolayzer Alternatif Yapılandırıcı Arkaışık Parlaklığı Önyükleme Onarımı Broadcom Yöneticisi Bu oturum için tuşeşlemini değiştir Zayıf Arkaplanı Değiştir Başlangıç Servislerini Seç Duvarkağıdı Seç Çözücü Yükleyicisi Otomatik bağlamayı yapılandır GPRS/UMTS Yapılandır Çalışan kalıcı Yapılandırma Bağlantı paylaşımlarını yapılandırma Çalışan-USB bölümlendirme oluşturun (UNetbootin) Görünümü Özelleştir Masaüstü Çevirmeli telefon Yapılandırma (GNOME PPP) Diskler Sürücüler Droopy (Dosya Paylaşımlı) Önyükleme menüsünü düzenle Ayar Dosyalarını Düzenle Dışarıda bırakılmış dosyaları düzenle Fluxbox Ayarlarını Düzenle IceWM Ayarlarını Düzenle JWM Ayarları Düzenle Sistem İzleyiciyi Düzenle (Conky) Güvenlik Duvarı Yapılandırması Donanım ISO Anlık Görüntüsü Bölüm görüntüsü antiX Linux'u Kur Canlı Live-USB Yapıcı (cli) Live-USB Yapıcı (Grafik Arayüz) Çalışan-usb çekirdek güncelleyici Bakım Paketleri Yönet Menü Düzenleyici Bağlı Aygıtları Tak Fare Yapılandırma Ağ Ağ Yardımcısı Ağ Arayüzleri (Ceni) Nvidia Sürücüsü Yükleyicisi PC Bilgileri Paket Yükleyicisi Bir Sürücüyü Bülümlendir Parola Komut İstemi(su/sudo) Tercih Edilen Uygulamalar Yazdırma Ayarları Canlı Yeniden Düzenle-Özelleştir Depo Yöneticisi SSH Parça Kalıcı root'u kaydedin Oturum Otomatik-giriş ayarla Tarih ve Saati Ayarla Yazı Tipi Boyutu Ayarı Grup Açılış Resmi (yalnızca png) Ekran Boşaltma Ayarı Ekran Çözünürlüğünü Ayarla (ARandR) Sistem Klavye Düzenini Ayarla Canlı kalıcı kur Paylaşılanlar Ses Kartı Seçici Dizinleri Eşitle Sistem Sistemi Yedekle Deneme Sesi Kullanıcı Masaüstü Oturumu Kullanıcı Yöneticisi WPA Yardımcısı Yapılandırma WiFi Bağlantı (Connman) Windows Kablosuz Sürücüleri 