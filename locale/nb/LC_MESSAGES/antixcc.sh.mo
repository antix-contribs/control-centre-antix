��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �               .     @     O     f     ~     �     �     �     �     �               ,     E      a     �  1   �     �  
   �  )   �               "     5     K     a     v     �     �  !   �     �  
   �  	   �          !     7     <     Q     f     �     �     �     �     �     �     �     �     
     "     1     P     a          �     �     �     �     �     �     �          #  &   9     `     s     �     �     �     �     �               #     ,     D     Y     s  '   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 13:38+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  DisconnectShares 1-til-1-hjelp 1-til-1 tale Oppsett av ADSL/PPPOE Reklameblokkering Tilpass mikser Alsamixer tonekontroll Oppsett av alternativer Bakbelysningens styrke Oppstartsreparasjon Broadcom-behandling Endre øktas tastaturoppsett Endre Slim-bakgrunn Velg oppstartstjenester Velg bakgrunn Installering av kodeker Oppsett av automontering Bredbåndsoppsett GPRS/UMTS Konfigurer lagring av brukerdata Oppsett av ConnectShares Opprett live-USB, behold partisjoner (UNetbootin) Tilpass utseende Skrivebord Oppsett av oppringt internett (GNOME PPP) Disker Drivere Droopy (fildeling) Rediger oppstartsmeny Rediger oppsettsfiler Rediger unntaksfiler Rediger Fluxbox Rediger IceWM-innstillinger Rediger JWM-innstillinger Rediger systemovervåking (Conky) Oppsett av brannmur Maskinvare ISO-bilde Ta diskbilde av partisjon Installer antiX Linux Live Live-USB Maker (CLI) Live-USB Maker (GUI) Live-USB kjerneoppdatering Vedlikehold Behandle pakker Menyredigering Monter lagringsenheter Oppsett av mus Nettverk Nettverksassistent Nettverksgrensesnitt (Ceni) Installer Nvidia-driver PC-informasjon Verktøy for pakkeinstallering Partisjoner disk Spør etter passord (su/sudo) Foretrukne programmer Utskriftsinnstillinger Remaster – tilpass live Behandle pakkearkiv SSH-Conduit Lagre systemdata Økt Velg auto-innlogging Endre tid og dato Velg skriftstørrelse Velg oppstartsbilde for Grub (kun PNG) Velg skjermsparing Velg skjermoppløsning (ARandR) Velg systemets tastaturoppsett Sett opp lagring av brukerdata Delte ressurser Velg lydkort Synkroniser mapper System Reservekopiering av system Test lyd Brukers skrivebordsøkt Brukeradministrasjon Oppsett av WPA-Supplicant Wi-Fi-tilkobling (Connman) Windows-drivere for trådløst nettverk 