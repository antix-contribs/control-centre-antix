# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-18 10:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: antixcc.sh:63
msgid "Desktop"
msgstr ""

#: antixcc.sh:63
msgid "Software"
msgstr ""

#: antixcc.sh:63
msgid "System"
msgstr ""

#: antixcc.sh:63
msgid "Network"
msgstr ""

#: antixcc.sh:63
msgid "Shares"
msgstr ""

#: antixcc.sh:63
msgid "Session"
msgstr ""

#: antixcc.sh:64
msgid "Live"
msgstr ""

#: antixcc.sh:64
msgid "Disks"
msgstr ""

#: antixcc.sh:64
msgid "Hardware"
msgstr ""

#: antixcc.sh:64
msgid "Drivers"
msgstr ""

#: antixcc.sh:64
msgid "Maintenance"
msgstr ""

#: antixcc.sh:65
msgid "Set Font Size"
msgstr ""

#: antixcc.sh:124
msgid "Edit Fluxbox Settings"
msgstr ""

#: antixcc.sh:129
msgid "Edit IceWM Settings"
msgstr ""

#: antixcc.sh:134
msgid "Edit JWM Settings"
msgstr ""

#: antixcc.sh:140
msgid "Edit Bootloader Menu"
msgstr ""

#: antixcc.sh:145
msgid "Edit Exclude Files"
msgstr ""

#: antixcc.sh:150 antixcc.sh:155
msgid "Manage Packages"
msgstr ""

#: antixcc.sh:161
msgid "Boot Repair"
msgstr ""

#: antixcc.sh:166 antixcc.sh:171 antixcc.sh:176
msgid "WiFi Connect (Connman)"
msgstr ""

#: antixcc.sh:182
msgid "Firewall Configuration"
msgstr ""

#: antixcc.sh:188
msgid "System Backup"
msgstr ""

#: antixcc.sh:194
msgid "Alsamixer Equalizer"
msgstr ""

#: antixcc.sh:200
msgid "Print Settings"
msgstr ""

#: antixcc.sh:206
msgid "Live-USB Kernel Updater"
msgstr ""

#: antixcc.sh:212
msgid "Set System Keyboard Layout"
msgstr ""

#: antixcc.sh:218
msgid "Choose Wallpaper"
msgstr ""

#: antixcc.sh:224
msgid "Edit System Monitor (Conky)"
msgstr ""

#: antixcc.sh:230
msgid "Customize Look and Feel"
msgstr ""

#: antixcc.sh:236
msgid "Preferred Applications"
msgstr ""

#: antixcc.sh:242
msgid "Package Installer"
msgstr ""

#: antixcc.sh:248
msgid "antiX Updater"
msgstr ""

#: antixcc.sh:254
msgid "antiX autoremove"
msgstr ""

#: antixcc.sh:260
msgid "Choose Startup Services"
msgstr ""

#: antixcc.sh:267
msgid "Set Date and Time"
msgstr ""

#: antixcc.sh:273
msgid "Network Interfaces (Ceni)"
msgstr ""

#: antixcc.sh:279
msgid "Select wifi Application"
msgstr ""

#: antixcc.sh:285
msgid "ConnectShares Configuration"
msgstr ""

#: antixcc.sh:291
msgid " DisconnectShares"
msgstr ""

#: antixcc.sh:297
msgid "Droopy (File Sharing)"
msgstr ""

#: antixcc.sh:303
msgid "1-to-1 Assistance"
msgstr ""

#: antixcc.sh:309
msgid "1-to-1 Voice"
msgstr ""

#: antixcc.sh:315
msgid "SSH Conduit"
msgstr ""

#: antixcc.sh:321
msgid "Dial-Up Configuaration (GNOME PPP)"
msgstr ""

#: antixcc.sh:327
msgid "WPA Supplicant Configuration"
msgstr ""

#: antixcc.sh:333
msgid "ADSL/PPPOE Configuration"
msgstr ""

#: antixcc.sh:339
msgid "Adblock"
msgstr ""

#: antixcc.sh:345
msgid "Login Manager"
msgstr ""

#: antixcc.sh:352
msgid "Change Slim Background"
msgstr ""

#: antixcc.sh:358
msgid "Set Grub Boot Image (png only)"
msgstr ""

#: antixcc.sh:363
msgid "Edit Config Files"
msgstr ""

#: antixcc.sh:369
msgid "Set Screen Resolution (ARandR)"
msgstr ""

#: antixcc.sh:375
msgid "Password Prompt (su/sudo)"
msgstr ""

#: antixcc.sh:381
msgid "Set Auto-Login"
msgstr ""

#: antixcc.sh:387
msgid "Set Screen Blanking"
msgstr ""

#: antixcc.sh:393
msgid "User Desktop-Session"
msgstr ""

#: antixcc.sh:399
msgid "Configure Automount"
msgstr ""

#: antixcc.sh:405
msgid "Mount Connected Devices"
msgstr ""

#: antixcc.sh:413
msgid "Live USB Maker (gui)"
msgstr ""

#: antixcc.sh:419
msgid "Live USB Maker (cli)"
msgstr ""

#: antixcc.sh:426
msgid "Install antiX Linux"
msgstr ""

#: antixcc.sh:432
msgid "Image a Partition"
msgstr ""

#: antixcc.sh:438
msgid "Synchronize Directories"
msgstr ""

#: antixcc.sh:444
msgid "Partition a Drive"
msgstr ""

#: antixcc.sh:456
msgid "PC Information"
msgstr ""

#: antixcc.sh:462
msgid "Mouse Configuration"
msgstr ""

#: antixcc.sh:468
msgid "Sound Card Chooser"
msgstr ""

#: antixcc.sh:474
msgid "Adjust Mixer"
msgstr ""

#: antixcc.sh:480
msgid "Nvidia Driver Installer"
msgstr ""

#: antixcc.sh:486
msgid "ISO Snapshot"
msgstr ""

#: antixcc.sh:492
msgid "Test Sound"
msgstr ""

#: antixcc.sh:498
msgid "Menu Editor"
msgstr ""

#: antixcc.sh:504
msgid "User Manager"
msgstr ""

#: antixcc.sh:510
msgid "Alternatives Configurator"
msgstr ""

#: antixcc.sh:516
msgid "Codecs Installer"
msgstr ""

#: antixcc.sh:522
msgid "Network Assistant"
msgstr ""

#: antixcc.sh:528
msgid "Repo Manager"
msgstr ""

#: antixcc.sh:534
msgid "Backlight Brightness"
msgstr ""

#: antixcc.sh:539
msgid "Save Root Persistence"
msgstr ""

#: antixcc.sh:544
msgid "Remaster-Customize Live"
msgstr ""
